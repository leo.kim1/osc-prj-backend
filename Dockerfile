FROM adoptopenjdk/openjdk11
ADD /build/libs/osc-prj-backend-0.0.1-SNAPSHOT.jar osc-prj-backend-0.0.1-SNAPSHOT.jar
ENV JAVA_OPTS=""
CMD ["java","-jar","osc-prj-backend-0.0.1-SNAPSHOT.jar"]
