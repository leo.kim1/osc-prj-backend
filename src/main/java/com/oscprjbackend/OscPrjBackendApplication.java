package com.oscprjbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OscPrjBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(OscPrjBackendApplication.class, args);
    }

}
