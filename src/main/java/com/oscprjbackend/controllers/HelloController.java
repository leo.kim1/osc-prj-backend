package com.oscprjbackend.controllers;


import com.oscprjbackend.dao.CustomerServiceRecordDao;
import com.oscprjbackend.domains.CustomerServiceRecord;
import com.oscprjbackend.dto.EEDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class HelloController {

    private CustomerServiceRecordDao csrDao;

    @Autowired
    public HelloController(CustomerServiceRecordDao csrDao) {
        this.csrDao = csrDao;
    }

    @GetMapping("/hello")
    @ApiOperation(value="hello", notes="Hello뒤에 name으로 받은 값을 리턴해줍니다.")
    public ResponseEntity<String> hello(@RequestParam(name = "name", defaultValue = "World") String name) {
        return ResponseEntity.ok(String.format("{\"Hello\":\"%s\"}", name));
    }

    @GetMapping("/customer_service_record_list")
    public List<CustomerServiceRecord> rTest(@RequestParam(name = "limit", defaultValue = "100") int limit) {
        // e
        List<CustomerServiceRecord> r = csrDao.getN(limit);
        return r;
    }
}
