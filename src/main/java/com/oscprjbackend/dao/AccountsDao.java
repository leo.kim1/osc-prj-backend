package com.oscprjbackend.dao;

import com.oscprjbackend.domains.Accounts;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class AccountsDao {

    public static final String TABLE_NAME ="accounts";
    private JdbcTemplate jdbcTemplate;

    public AccountsDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private RowMapper<Accounts> rowMapper = new RowMapper<Accounts>() {
        @Override
        public Accounts mapRow(ResultSet rs, int rowNum) throws SQLException {
            Accounts accounts = new Accounts();
            return accounts;
        }
    };

    public void add(Accounts accounts) {
        String query = String.format("insert into %s(username, password, email, createdAt, updatedAt) values(?, ?, ?, ?, ?)", TABLE_NAME);
        this.jdbcTemplate.update(query, accounts.getUsername(),
                accounts.getPassword(),
                accounts.getEmail(),
                Timestamp.valueOf(accounts.getCreatedAt()),
                Timestamp.valueOf(accounts.getUpdatedAt())
        );
    }

    public void deleteAll() {
        String sql = String.format("delete from %s", TABLE_NAME);
        this.jdbcTemplate.execute(sql);
    }


    public Integer countAll() {
        String sql = String.format("select count(*) from %s", TABLE_NAME);
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }



}
