package com.oscprjbackend.dao;

import com.oscprjbackend.domains.CustomerServiceRecord;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class CustomerServiceRecordDao {

    public static final String TABLE_NAME ="customer_service_record";

    private JdbcTemplate jdbcTemplate;

    private RowMapper<CustomerServiceRecord> rowMapper = new RowMapper<CustomerServiceRecord>() {
        @Override
        public CustomerServiceRecord mapRow(ResultSet rs, int rowNum) throws SQLException {
            CustomerServiceRecord customerServiceRecord = new CustomerServiceRecord(
                rs.getLong("id"),
                rs.getInt("car_model_id"),
                rs.getTimestamp("cs_start_date").toLocalDateTime(),
                rs.getString("contents")
            );
            return customerServiceRecord;
        }
    };

    public CustomerServiceRecordDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int createTable() {
        // table이 있는지 먼저 check
        return this.jdbcTemplate.update(String.format("create table if not exists " +
                "customer_service_record(id bigint, car_model_id int, cs_start_date timestamp," +
                " contents text, PRIMARY KEY (id))"));
    }

    public void dropTable() {
        // table이 있는지 먼저 check
        this.jdbcTemplate.execute("drop table if exists customer_service_record");
    }

    public void add(CustomerServiceRecord customerServiceRecord) {
        String query = String.format("insert into %s(id, car_model_id, cs_start_date, contents) values(?, ?, ?, ?)", TABLE_NAME);
        this.jdbcTemplate.update(query, customerServiceRecord.getId(),
                customerServiceRecord.getCarModelId(),
                Timestamp.valueOf(customerServiceRecord.getCsStartDate()),
                customerServiceRecord.getContents());
    }

    public CustomerServiceRecord get(long id) {
        String sql = String.format("select * from %s where id = ?", TABLE_NAME);
        return this.jdbcTemplate.queryForObject(sql, rowMapper, id);
    }

    public List<CustomerServiceRecord> getN(int limit) {
        String sql = String.format("select * from %s limit ?", TABLE_NAME);
        return this.jdbcTemplate.query(sql, rowMapper, limit);
    }

    public void deleteAll() {
        String sql = String.format("delete from %s", TABLE_NAME);
        this.jdbcTemplate.execute(sql);
    }
}
