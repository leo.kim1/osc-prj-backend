package com.oscprjbackend.dao;

import java.util.Scanner;

public class Main {
    public void not(int n) {
        System.out.println(~n);
    }

    public void and(int a, int b) {
        System.out.println(a & b);
    }

    public void or(int a, int b) {
        System.out.println(a | b);
    }

    public void xor(int a, int b) {
        System.out.println(a ^ b);
    }

    public String[] getSplittedStr() {
        Scanner sc = new Scanner(System.in);
        String a = sc.nextLine();
        String[] sp = a.split(" ");
        return sp;
    }

    public static void main(String[] args) {
//        Main m = new Main();
//        String[] sp = m.getSplittedStr();
//        m.xor(Integer.parseInt(sp[0]), Integer.parseInt(sp[1]));

        Scanner sc = new Scanner(System.in);
        String a = sc.nextLine();
        System.out.println(Long.parseLong(a));
    }
}
