package com.oscprjbackend.domains;

import java.time.LocalDateTime;

public class CustomerServiceRecord {
    private long id;
    private int carModelId;
    private LocalDateTime csStartDate;
    private String contents;

    public CustomerServiceRecord(long id, int carModelId, LocalDateTime csStartDate, String contents) {
        this.id = id;
        this.carModelId = carModelId;
        this.csStartDate = csStartDate;
        this.contents = contents;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCarModelId() {
        return carModelId;
    }

    public void setCarModelId(int carModelId) {
        this.carModelId = carModelId;
    }

    public LocalDateTime getCsStartDate() {
        return csStartDate;
    }

    public void setCsStartDate(LocalDateTime csStartDate) {
        this.csStartDate = csStartDate;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }
}
