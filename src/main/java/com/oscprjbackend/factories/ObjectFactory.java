package com.oscprjbackend.factories;

import com.oscprjbackend.dao.AccountsDao;
import com.oscprjbackend.dao.CustomerServiceRecordDao;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.metadata.HikariDataSourcePoolMetadata;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;


import javax.sql.DataSource;

@Configuration
public class ObjectFactory {

    @Value("${DB_HOST1}")
    private String dbHost;

    @Value("${DB_USERNAME1}")
    private String dbUserName;

    @Value("${DB_PASSWORD1}")
    private String dbPassword;

    @Bean
    public AccountsDao accountsDao() {
        return new AccountsDao(new JdbcTemplate(dataSourceHikari()));
    }

    @Bean
    public CustomerServiceRecordDao csrDao() {
        return new CustomerServiceRecordDao(new JdbcTemplate(dataSource()));
    }

    @Bean
    public CustomerServiceRecordDao csrTestDao() {
        return new CustomerServiceRecordDao(new JdbcTemplate(dataSourceForTest()));
    }

    public DataSource dataSource() {
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(org.postgresql.Driver.class);
        System.out.println(String.format("dbHost: %s", dbHost));
        System.out.println(String.format("dbPassword: %s", dbPassword));
        dataSource.setUrl(dbHost);
        dataSource.setUsername(dbUserName);
        dataSource.setPassword(dbPassword);
        return dataSource;
    }

    public DataSource dataSourceForTest() {
        SingleConnectionDataSource dataSource = new SingleConnectionDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        // DB_USERNAME=;DB_PASSWORD=123456;DB_HOST=jdbc:postgresql://ec2-54-180-78-236.ap-northeast-2.compute.amazonaws.com:5432/aims_core
        dataSource.setUrl("jdbc:postgresql://ec2-54-180-78-236.ap-northeast-2.compute.amazonaws.com:5432/aims_core");
        dataSource.setUsername("postgres");
        dataSource.setPassword("");
        return dataSource;
    }

    public DataSource dataSourceHikari() {
        HikariConfig hc = new HikariConfig();
        hc.setDriverClassName("org.postgresql.Driver");
        hc.setJdbcUrl("jdbc:postgresql://<host>:9999/testdb");
        hc.setUsername("postgres");
        hc.setPassword("");
        hc.setConnectionTimeout(40000); // 디폴트 30000(30초)
        hc.setMaximumPoolSize(100);


        DataSource dataSource = new HikariDataSource(hc);
        return dataSource;
    }


    public DataSource dataSourceForSolum() {
        SingleConnectionDataSource dataSource = new SingleConnectionDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://<host>:9999/testdb");
        dataSource.setUsername("postgres");
        dataSource.setPassword("");
        return dataSource;
    }

    public DataSource dataSourceForLocal() {
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(org.postgresql.Driver.class);
        // DB_USERNAME=;DB_PASSWORD=123456;DB_HOST=jdbc:postgresql://ec2-54-180-78-236.ap-northeast-2.compute.amazonaws.com:5432/aims_core
        dataSource.setUrl("jdbc:postgresql://localhost:5432/spring_example");
        dataSource.setUsername("postgres");
        dataSource.setPassword("");
        return dataSource;
    }
}
