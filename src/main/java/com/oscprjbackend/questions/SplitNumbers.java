package com.oscprjbackend.questions;

import java.util.ArrayList;
import java.util.List;

public class SplitNumbers {

    // 54321 / 10 = 5432
    // 5432 / 10 = 543
    // ...
    // 54 / 10 = 5
    // 5 / 10 = 안나눠짐
    public static void main(String[] args) {
        long n = 54321;
        int[] answer = new int[(int) (Math.log10(n) + 1)];
        int idx = 0;
        while (n > 0) {
            answer[idx++] = (int) (n % 10);
            n /= 10;
        }
        for (int i = 0; i < answer.length; i+=1) {
            System.out.printf(String.format("%s", answer[i]));
        }
    }
}

