package com.oscprjbackend.dao;

import com.oscprjbackend.domains.Accounts;
import com.oscprjbackend.factories.ObjectFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ObjectFactory.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AccountsDaoTest {

    @Autowired
    private AccountsDao accountsDao;

    @Test
    void addAndGet() {
        int errCnt = 0;
        accountsDao.deleteAll();

        for (int i = 0; i < 6000; i++) {
            try {
                accountsDao.add(new Accounts("kyeongrok"+i, "1234", "kyeongork@osckorea.com", LocalDateTime.now(), LocalDateTime.now()));
                if (i != 0 && i % 10 == 0) {
                    int cnt = accountsDao.countAll();
                    System.out.println(String.format("errcnt: %d countAll:%d", errCnt, cnt));
                }
            } catch (Exception e) {
                e.printStackTrace();
                errCnt += 1;
                System.out.printf("------ an error occured on %d ---------%n", i);
            }
        }
        System.out.printf("countallf: %d%n", accountsDao.countAll());
        System.out.println(errCnt);
        System.out.println(String.format("err cnt :%d", errCnt));
    }
}