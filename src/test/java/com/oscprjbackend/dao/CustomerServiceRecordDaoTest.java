package com.oscprjbackend.dao;

import com.oscprjbackend.domains.CustomerServiceRecord;
import com.oscprjbackend.factories.ObjectFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ObjectFactory.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CustomerServiceRecordDaoTest {

    private List<CustomerServiceRecord> customerServiceRecords;

    @Autowired
    private CustomerServiceRecordDao csrTestDao;

    @BeforeAll
    public void init() {
        customerServiceRecords = Arrays.asList(
            new CustomerServiceRecord(0, 1, LocalDateTime.now(), "200만건 데이터 유실 없는지"),
            new CustomerServiceRecord(1, 1, LocalDateTime.now(), "200만건 데이터 정해진 시간에 잘 끝나는지")

        );
    }

    @Test
    void createTableTest() {
        csrTestDao.dropTable();
        csrTestDao.createTable();
    }

    @Test
    void addAndGet() {
        csrTestDao.add(new CustomerServiceRecord(0, 1, LocalDateTime.now(), "heekjekjkejekjekjekje"));
        CustomerServiceRecord csr = csrTestDao.get(0);
        assertEquals(0, csr.getId());
        assertEquals(1, csr.getCarModelId());
    }

    @Test
    void add() {
        csrTestDao.deleteAll();
        csrTestDao.add(customerServiceRecords.get(0));
        csrTestDao.add(customerServiceRecords.get(1));
        CustomerServiceRecord csr = csrTestDao.get(0);
        assertEquals(0, csr.getId());
        assertEquals(1, csr.getCarModelId());
    }
}